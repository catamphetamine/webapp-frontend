import React, { useCallback } from 'react'
import PropTypes from 'prop-types'
import { useSelector, useDispatch } from 'react-redux'
import { Modal } from 'react-responsive-ui'

import Button from './Button'

import { close } from '../redux/okCancelDialog'
import { getPromise, resolve } from './OkCancelDialogPromise'

import './OkCancelDialog.css'

export default function OkCancelDialog({
	okLabel,
	cancelLabel,
	yesLabel,
	noLabel
}) {
	const dispatch = useDispatch()

	const isOpen = useSelector(({ okCancelDialog }) => okCancelDialog.isOpen)
	const title = useSelector(({ okCancelDialog }) => okCancelDialog.title)
	const content = useSelector(({ okCancelDialog }) => okCancelDialog.content)
	const input = useSelector(({ okCancelDialog }) => okCancelDialog.input)

	const onClose = useCallback((value) => {
		resolve(value)
		dispatch(close())
	}, [dispatch])

	const onOk = useCallback(() => onClose(true), [onClose])
	const onCancel = useCallback(() => onClose(false), [onClose])

	return (
		<Modal
			isOpen={isOpen}
			close={onClose}>
			{title &&
				<Modal.Title>
					{title}
				</Modal.Title>
			}
			<Modal.Content>
				{content}
			</Modal.Content>
			<Modal.Actions className="form__actions">
				<Button
					style="text"
					onClick={onCancel}
					className="form__action">
					{input ? cancelLabel : noLabel}
				</Button>
				<Button
					onClick={onOk}
					style="fill"
					className="form__action">
					{input ? okLabel : yesLabel}
				</Button>
			</Modal.Actions>
		</Modal>
	)
}

OkCancelDialog.getPromise = getPromise

OkCancelDialog.propTypes = {
	// isOpen: PropTypes.bool,
	// close: PropTypes.func.isRequired,
	// title: PropTypes.string,
	// content: PropTypes.node,
	// input: PropTypes.bool,
	okLabel: PropTypes.string.isRequired,
	cancelLabel: PropTypes.string.isRequired,
	yesLabel: PropTypes.string.isRequired,
	noLabel: PropTypes.string.isRequired
}