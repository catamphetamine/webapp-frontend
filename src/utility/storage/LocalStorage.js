// https://developer.mozilla.org/en-US/docs/Web/API/Storage

export function hasObject(key) {
	return localStorage.getItem(key) !== null
}

export function getObject(key, defaultValue) {
	const value = localStorage.getItem(key)
	if (value === null) {
		return defaultValue
	}
	try {
		return JSON.parse(value)
	} catch (error) {
		if (error instanceof SyntaxError) {
			console.error(`Invalid JSON:\n\n${value}`)
			return defaultValue
		} else {
			throw error
		}
	}
}

export function setObject(key, value) {
	if (value === undefined) {
		deleteObject(key)
	} else {
		localStorage.setItem(key, JSON.stringify(value))
	}
}

export function deleteObject(key) {
	localStorage.removeItem(key)
}

// https://blog.bitsrc.io/4-ways-to-communicate-across-browser-tabs-in-realtime-e4f5f6cbedca
// "... this approach has adverse effects since LocalStorage is synchronous. And hence can block the main UI thread."
export function addStorageListener(key, listener) {
	window.addEventListener('storage', (event) => {
		if (event.storageArea !== localStorage) {
			return
		}
		if (event.key === key) {
			// { oldValue, newValue, url }
			// https://developer.mozilla.org/en-US/docs/Web/API/StorageEvent
			if (event.newValue) {
				listener(JSON.parse(event.newValue))
			} else {
				listener()
			}
		}
	})
}

function getKeys() {
	const keys = []
	let i = 0
	while (i < localStorage.length) {
		keys.push(localStorage.key(i))
		i++
	}
	return keys
}

export default class LocalStorage {
	has(key) {
		return hasObject(key)
	}
	get(key, defaultValue) {
		debug(`read "${key}"`)
		return getObject(key, defaultValue)
	}
	set(key, value) {
		debug(`write "${key}"`)
		setObject(key, value)
	}
	delete(key) {
		debug(`delete "${key}"`)
		deleteObject(key)
	}
	keys() {
		return getKeys()
	}
	// Listens for changes to `localStorage`.
	// https://developer.mozilla.org/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API#Responding_to_storage_changes_with_the_StorageEvent
	// https://developer.mozilla.org/docs/Web/API/StorageEvent
	onChange(_listener) {
		const listener = (event) => {
			_listener({ key: event.key })
		}
		window.addEventListener('storage', listener)
		return () => {
			window.removeEventListener('storage', listener)
		}
	}
}

function debug(message) {
	console.log(`[LocalStorage] ${message}`)
}