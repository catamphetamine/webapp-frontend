module.exports = (api) => {
	const reactPlugins = []
	// Fixes "$RefreshReg$ is not defined" bug of `react-refresh`.
	// https://github.com/pmmmwh/react-refresh-webpack-plugin/issues/176#issuecomment-782770175
	if (api.env('development')) {
		reactPlugins.push('react-refresh/babel')
	} else {
		reactPlugins.push(['babel-plugin-transform-react-remove-prop-types', { removeImport: true }])
	}
	return {
		presets: [
			"@babel/preset-env"
		],
		plugins: [
			["@babel/plugin-proposal-decorators", { "legacy": true }],
			"@babel/plugin-proposal-class-properties"
		],
		overrides: [{
			include: [
				"./src",
				"../webapp-frontend/src"
			],
			presets: [
				"@babel/preset-react"
			],
			plugins: reactPlugins
		}, {
			include: "./rendering-service",
			presets: [
				"@babel/preset-react"
			]
		}]
	}
}